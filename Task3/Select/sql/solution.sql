--Задание 1. Найдите номер модели, скорость и размер жесткого диска для всех ПК,
--стоимостью менее 500 дол. Вывести: model, speed и hd
select model, speed, hd
from PC
where PC.price < 500;

--Задание 2. Найдите производителей принтеров. Вывести: maker
select distinct(maker) from Product where type = 'Printer';

--Здание 3. Найдите номер модели, объем памяти и размеры экранов ПК-блокнотов, цена которых превышает 1000 дол.
select model, ram, screen from Laptop where price > 1000;

--Задание 4. Найдите все записи таблицы Printer для цветных принтеров.
select * from Printer where color = 'y';

--Задание 5. Найдите номер модели, скорость и размер жесткого диска ПК, имеющих 12x или 24x CD и цену менее 600 дол.
Select model, speed, hd from PC
where (cd = '12x' or cd = '24x') and price < 600;

--Задание 6. Для каждого производителя, выпускающего ПК-блокноты c объёмом жесткого диска не менее 10 Гбайт, найти скорости таких ПК-блокнотов. Вывод: производитель, скорость.
select distinct Product.maker, Laptop.speed
from Product, Laptop
where Laptop.model = Product.model and Laptop.hd >= 10 and Product.type = 'laptop';

--Задание 7. Найдите номера моделей и цены всех имеющихся в продаже продуктов (любого типа) производителя B (латинская буква).
select model, price
from PC
where model = (SELECT model
               from Product
               where maker = 'B' and
                       type = 'PC'
)
union
select model, price
from Laptop
where model = (SELECT model
               from Product
               where maker = 'B' and
                       type = 'Laptop'
)
union
select model, price
from Printer
where model = (SELECT model
               from Product
               where maker = 'B' and
                       type = 'Printer'
);
--Задание 8. Найдите производителя, выпускающего ПК, но не ПК-блокноты.
select distinct maker
from Product
where type = 'pc'
    except
select distint Product.maker
from Product
where type = 'Laptop';

--Задание 9. Найдите производителей ПК с процессором не менее 450 Мгц. Вывести: Maker
select distinct Product.maker
from PC
         inner join Product on PC.model = Product.model
where PC.speed >= 450;

--Задание 10. Найдите модели принтеров, имеющих самую высокую цену. Вывести: model, price
select model, price
from Printer
where price =(select max(price) from Printer );

--Задание 11. Найдите среднюю скорость ПК.
select avg(speed) from PC;

--Задание 12. Найдите среднюю скорость ПК-блокнотов, цена которых превышает 1000 дол.
select avg(speed)
from Laptop
where price > 1000;

--Задание 13. Найдите среднюю скорость ПК, выпущенных производителем A.
select avg(PC.speed)
from PC, Product
where PC.model = Product.model and Product.maker = 'A';

--Задание 14. Найти производителей, которые выпускают более одной модели, при этом все выпускаемые производителем модели являются продуктами одного типа. Вывести: maker, type
select maker, max(type)
from Product
group by maker
having count(distinct type) = 1 and count(model) > 1;

--Задание 15. Найдите размеры жестких дисков, совпадающих у двух и более PC. Вывести: HD
SELECT hd FROM PC GROUP BY (hd) HAVING COUNT(model) >= 2;

--Задание 16. Найдите пары моделей PC, имеющих одинаковые скорость и RAM. В результате каждая пара указывается только один раз, т.е. (i,j), но не (j,i), Порядок вывода: модель с большим номером,
--модель с меньшим номером, скорость и RAM.
select distinct p1.model, p2.model, p1.speed, p1.ram
from pc p1, pc p2
where p1.speed = p2.speed AND p1.ram = p2.ram AND p1.model > p2.model;

--Задание 17. Найдите модели ПК-блокнотов, скорость которых меньше скорости любого из ПК.Вывести: type, model, speed
select distinct p.type,p.model,l.speed
from Laptop l
         join Product p on l.model=p.model
where l.speed<(select min(speed)
               from PC);

--Задание 18. Найдите производителей самых дешевых цветных принтеров. Вывести: maker, price
select distinct Product.maker, Printer.price
from Product, Printer
where Product.model = Printer.model
  and Printer.color = 'y'
  and Printer.price = (
    select min(price) from Printer
    where printer.color = 'y'
);

--Задание 19. Для каждого производителя, имеющего модели в таблице Laptop, найдите средний размер экрана выпускаемых им ПК-блокнотов.
--Вывести: maker, средний размер экрана.
select Product.maker, avg(screen)
from laptop
    left join Product on Product.model = Laptop.model
group by Product.maker;

--Задание 20. Найдите производителей, выпускающих по меньшей мере три различных модели ПК. Вывести: Maker, число моделей ПК.
select maker, cout(model)
from Product
where type = 'pc'
group by Product.maker
having count (distinct model) >= 3;

--Задание 21. Найдите максимальную цену ПК, выпускаемых каждым производителем, у которого есть модели в таблице PC. Вывести: maker, максимальная цена.
select Product.maker, max(PC.price)
from Product, PC
where Product.model = PC.model
group by Product.maker;

--Задание 22. Для каждого значения скорости ПК, превышающего 600 МГц, определите среднюю цену ПК с такой же скоростью. Вывести: speed, средняя цена.
select PC.speed, avg(PC.price)
from PC
where PC.speed > 600
group by PC.speed;

--Задание 23. Найдите производителей, которые производили бы как ПК со скоростью не менее 750 МГц,
-- так и ПК-блокноты со скоростью не менее 750 МГц.Вывести: Maker
select distinct maker
from Product t1 join pc t2 on t1.model=t2.model
where speed>=750 and maker in
                     ( select maker
                       from Product t1 join laptop t2 on t1.model=t2.model
                       where speed>=750 );



--Задание 24. Перечислите номера моделей любых типов, имеющих самую высокую цену по всей имеющейся в базе данных продукции.
select model
from (
    select model, price
    from PC
    union
    select model, price
    from Laptop
    union
    select model, price
    from Printer
    ) t1
where price = (
    select max(price)
    from (
    select price
    from PC
    union
    select price
    from Laptop
    union
    select price
    from Printer
    ) t2
);

--Задание 25. Найдите производителей принтеров, которые производят ПК с наименьшим объемом RAM
-- и с самым быстрым процессором среди всех ПК, имеющих наименьший объем RAM. Вывести: Maker
select distinct maker
from Product
where model in (
    select model
    from PC
    where ram = (
        select min(ram)
        from PC
    )
      and speed = (
        select max(speed)
        from PC
        where ram = (
            select min(ram)
            from PC
        )
    )
)
  and
        maker in (
        select maker
        from Product
        where type='Printer'
    );

--Задание 26. Найдите среднюю цену ПК и ПК-блокнотов, выпущенных производителем A (латинская буква). Вывести: одна общая средняя цена.
SELECT sum(s.price)/sum(s.kol) as sredn FROM
    (SELECT price,1 as kol FROM pc,product
    WHERE PC.model = Product.model AND Product.maker = 'A'
    UNION all
    SELECT price,1 as kol FROM Laptop,Product
    WHERE Laptop.model = Product.model AND Product.maker= 'A') as s;

--Задание 27. Найдите средний размер диска ПК каждого из тех производителей, которые выпускают и принтеры. Вывести: maker, средний размер HD.
SELECT product.maker, AVG(pc.hd)
FROM pc, product
WHERE product.model = pc.model AND product.maker IN (
    SELECT DISTINCT maker
    FROM product
    WHERE product.type = 'printer')
GROUP BY maker;

--Задание 28. Найдите средний размер диска ПК (одно значение для всех) тех производителей, которые выпускают и принтеры. Вывести: средний размер HD
SELECT AVG(pc.hd)
FROM pc, product
WHERE product.model = pc.model
  AND product.maker IN (SELECT DISTINCT maker
                        FROM product WHERE product.type = 'printer');

--Задание 29. В предположении, что приход и расход денег на каждом пункте приема фиксируется не чаще одного раза в день [т.е. первичный ключ (пункт, дата)], написать запрос с выходными данными (пункт, дата, приход, расход). Использовать таблицы Income_o и Outcome_o.
SELECT t1.point, t1.date, inc, out
FROM income_o t1 LEFT JOIN outcome_o t2 ON t1.point = t2.point
    AND t1.date = t2.date
UNION
SELECT t2.point, t2.date, inc, out
FROM income_o t1 RIGHT JOIN outcome_o t2 ON t1.point = t2.point
    AND t1.date = t2.date;

--Задание 30. В предположении, что приход и расход денег на каждом пункте приема фиксируется произвольное число раз (первичным ключом в таблицах является столбец code), требуется получить таблицу, в которой каждому пункту за каждую дату выполнения операций будет соответствовать одна строка.
--Вывод: point, date, суммарный расход пункта за день (out), суммарный приход пункта за день (inc).
--Отсутствующие значения считать неопределенными (NULL).
select point, date, SUM(sum_out), SUM(sum_inc)
from( select point, date, SUM(inc) as sum_inc, null as sum_out from Income Group by point, date
    Union
    select point, date, null as sum_inc, SUM(out) as sum_out from Outcome Group by point, date ) as t
group by point, date order by point;

--Задание 31 Для классов кораблей, калибр орудий которых не менее 16 дюймов, укажите класс и страну.
SELECT DISTINCT class, country
FROM classes
WHERE bore >= 16;

--Задание 32. Одной из характеристик корабля является половина куба калибра его главных орудий (mw).
-- С точностью до 2 десятичных знаков определите среднее значение mw для кораблей каждой страны,
-- у которой есть корабли в базе данных.
Select country, cast(avg((power(bore,3)/2)) as numeric(6,2)) as weight
from (select country, classes.class, bore, name from classes left join ships on classes.class=ships.class
    union all
    select distinct country, class, bore, ship from classes t1 left join outcomes t2 on t1.class = t2.ship
    where ship = class and ship not in (select name from ships) ) a
where name IS NOT NULL group by country

--Задание 33. Укажите корабли, потопленные в сражениях в Северной Атлантике (North Atlantic). Вывод: ship.
SELECT o.ship
FROM BATTLES b
    LEFT join outcomes o ON o.battle = b.name
WHERE b.name = 'North Atlantic' AND o.result = 'sunk';

--Задание 34. По Вашингтонскому международному договору от начала 1922 г. запрещалось строить линейные корабли
-- водоизмещением более 35 тыс.тонн. Укажите корабли, нарушившие этот договор
-- (учитывать только корабли c известным годом спуска на воду). Вывести названия кораблей.
Select name
from classes, ships
where launched >= 1922
  and displacement > 35000
  and type = 'bb'
  and ships.class = classes.class;

--Задание 35. В таблице Product найти модели, которые состоят только из цифр или только из латинских букв
-- (A-Z, без учета регистра).
--Вывод: номер модели, тип модели.
SELECT model, type
FROM Product
WHERE upper(model) NOT like '%[^A-Z]%'
   OR model not like '%[^0-9]%';

--Задание 36. Перечислите названия головных кораблей, имеющихся в базе данных (учесть корабли в Outcomes).
Select name
from ships
where class = name
union
select ship as name
from classes,outcomes
where classes.class = outcomes.ship;

--Задание 37. Найдите классы, в которые входит только один корабль из базы данных (учесть также корабли в Outcomes).
SELECT c.class
FROM classes c
    LEFT JOIN (
    SELECT class, name
    FROM ships
    UNION
    SELECT ship, ship
    FROM outcomes
    ) AS s ON s.class = c.class
GROUP BY c.class
HAVING COUNT(s.name) = 1;

--Задание 38. Найдите страны, имевшие когда-либо классы обычных боевых кораблей ('bb') и имевшие когда-либо классы крейсеров ('bc').
SELECT country
FROM classes
GROUP BY country
HAVING COUNT(DISTINCT type) = 2;

--Задание 39. Найдите корабли, "сохранившиеся для будущих сражений";
-- т.е. выведенные из строя в одной битве (damaged), они участвовали в другой, произошедшей позже.
WITH b_s AS
(SELECT o.ship, b.name, b.date, o.result
FROM outcomes o
LEFT JOIN battles b ON o.battle = b.name )
SELECT DISTINCT a.ship FROM b_s a
WHERE UPPER(a.ship) IN
      (SELECT UPPER(ship) FROM b_s b
       WHERE b.date < a.date AND b.result = 'damaged');

--Задание 40. Найдите класс, имя и страну для кораблей из таблицы Ships, имеющих не менее 10 орудий.
SELECT s.class, s.name, c.country
FROM ships s
    LEFT JOIN classes c ON s.class = c.class
WHERE c.numGuns >= 10w;
