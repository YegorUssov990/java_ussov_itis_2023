import jdbc.SimpleDataSource;
import models.Student;
import repositories.StudentsRepository;
import repositories.StudentsRepositoryJdbcImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("resources\\db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(dataSource);

        System.out.println(studentsRepository.findAll());
        System.out.println(studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(21));
        System.out.println(studentsRepository.delete(Long.valueOf(1)));
        System.out.println(studentsRepository.update(Long.valueOf(1),new Student("Марсель", "Сидиков", 27)));
    }

}
