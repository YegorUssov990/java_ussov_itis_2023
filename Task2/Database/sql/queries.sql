--получение всех данных
select * from users;
select * from drivers;
select * from cars;
select * from orders;
-- получение данных из таблицы, отсортированных по убыванию возраста
select * from users order by firstName desc, lastName;
--получение только имени и фамилии
select firstName, lastName from users;
