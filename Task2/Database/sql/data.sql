insert into users(firstName, lastName, email, password) values ('Марсель', 'Сидиков', 'rfwefwfww', 'qweqwre212');
insert into users(firstName, lastName, email, password) values ('Усов', 'Егор', '324242ук', 'qweqwre213');
insert into users(firstName, lastName, email, password) values ('Петухов', 'Даниил', '23424', 'qweqwre214');

insert into drivers(firstName, lastName, experience, password) values
                                                                   ('Артем', 'Степкин', 5, 'qweqwre212'),
                                                                   ('Ваня', 'Ухин', 6, 'qweqwre213'),
                                                                   ('Олег', 'Носин', 7, 'qweqwre214');

insert into cars(brand, color, number) values
                                           ('Nissan', 'black', '245'),
                                           ('Honda', 'white', '666'),
                                           ('Infinity', 'black', '333');

insert into orders(numberOrder, driverName, carNumber, time) values
                                                                 (1, 'Артем', '245', '18:00'),
                                                                 (2, 'Ваня', '666', '19:00'),
                                                                 (3, 'Олег', '333', '20:00');

--обновление данных в таблице
update users set phoneNumber = '89228250585'git where id = 2;
update drivers set phoneNumber = '89228250585' where id = 3;
update cars set ageCar = '5' where brand = 'Nissan';
update orders set data = '09/04/2022' where driverName = 'Артем';