drop table if exists users;
drop table if exists drivers;
drop table if exists orders;
drop table if exists cars;


create table users (
                       id bigserial primary key,
                       firstName char(20),
                       lastName char(20),
                       email char(30),
                       password char(30)
);

create table drivers (
                         id bigserial primary key,
                         firstName char(20),
                         lastName char(20),
                         experience integer,
                         password char(30)
);

create table cars (
                      carId bigserial primary key,
                      brand char(20),
                      color char(20),
                      number char(30)
);

create table orders (
                        numberOrder bigint unique not null ,
                        driverName char(20),
                        carNumber char(20),
                        time char(10)
);

--изменение таблицы(добавлене столбца)
alter table users add phoneNumber char(20) not null default '';
alter table drivers add phoneNumber char(20) not null default '';
alter table cars add ageCar char(20) not null default '';
alter table orders add data char(20) not null default '';

--изменение таблицы(добавление нового ключа)
alter table users add foreign key (id) references orders(numberOrder);
alter table drivers add foreign key (id) references orders(numberOrder);
alter table drivers add foreign key (id) references cars(carId);
