package repositories;

import models.Student;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * 08.07.2022
 * 03. Database
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class StudentsRepositoryJdbcImpl implements StudentsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL_STUDENTS = "select * from student;";
    public static final String SQL_SELECT_USER_ID =
            "SELECT * FROM users WHERE id=?";

    private static final Function<ResultSet, Student> studentMapper = row -> {

        try {
            Long id = row.getLong("id");
            String firstName = row.getString("first_name");
            String lastName = row.getString("last_name");
            Integer age = row.getObject("age", Integer.class);
            return new Student(id, firstName, lastName, age);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    };

    private final DataSource dataSource;

    public StudentsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Student> findAll() {

        List<Student> students = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {

            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_STUDENTS)) {
                while (resultSet.next()) {
                    students.add(studentMapper.apply(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        return students;
    }

    @Override
    public void save(Student student) {
            try (Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement()) {
                int rows = statement.executeUpdate("INSERT Student(firstName, lastName, age)" +
                        " VALUES ('Федя', 'Петров', '19' );");
            }
            catch(SQLException e){
                throw new IllegalArgumentException();
            }
        }

    @Override
    public Optional<Student> findById(Long id) {
        Optional<Student> student = null;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement =
                     connection.prepareStatement(SQL_SELECT_USER_ID)) {
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                String name = rs.getString(2);
                Optional<Student> student1 = Optional.of(new Student(student.get().getFirstName(), student.get().getLastName(), student.get().getAge()));
            }
        } catch (SQLException e) {
          throw new IllegalArgumentException;
        }
        return Optional.empty();
    }
}
