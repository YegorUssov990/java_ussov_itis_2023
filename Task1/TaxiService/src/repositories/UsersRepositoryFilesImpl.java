package repositories;

import models.User;

import java.io.*;
import java.util.*;
import java.util.function.Function;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;
    private long counter;

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
        this.counter = 0;
    }

    public List<User> findAll() {
        List<User> lst = new LinkedList<>();
        String line = null;
        try {;
            File sourceFile = new File(fileName);
            BufferedReader reader = new BufferedReader(new FileReader(sourceFile));;
            while ((line = reader.readLine()) != null) {
                String[] de = line.split("\\|");
                lst.add(new User(UUID.fromString(de[0]), de[1], de[2], de[3], de[4]));
            }
        }catch (IOException  e) {
            e.printStackTrace();
        }
        return lst;
    }

    @Override
    public void save(User entity) {
        entity.setId(UUID.randomUUID());
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        String line = null;
        try {
            File sourceFile = new File(fileName);
            File outputFile = new File("users2.txt");
            BufferedReader reader = new BufferedReader(new FileReader(sourceFile));
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
            while ((line = reader.readLine()) != null) {
                String[] de = line.split("\\|");
                if (de[0].equals(entity.getId())) {
                    de[1] = entity.getFirstName();
                    de[2] = entity.getLastName();
                    de[3] = entity.getEmail();
                    de[4] = entity.getPassword();

                    writer.write(line);
                    writer.newLine();
                }
            }
            reader.close();
            writer.close();
            sourceFile.delete();
            outputFile.renameTo(sourceFile);
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void delete1(User entity) {
        String line = null;
        try {
            File sourceFile = new File(fileName);
            File outputFile = new File("users2.txt");
            BufferedReader reader = new BufferedReader(new FileReader(sourceFile));
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
            while ((line = reader.readLine()) != null) {
                String[] de = line.split("\\|");
                if (de[1].equals(entity.getFirstName())
                        || de[2].equals(entity.getLastName())) {
                    writer.write(line);
                    writer.newLine();
                }
            }
            reader.close();
            writer.close();
            sourceFile.delete();
            outputFile.renameTo(sourceFile);
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void deleteById(UUID id) {
        String line = null;
        try {
            File sourceFile = new File(fileName);
            File outputFile = new File("users2.txt");
            BufferedReader reader = new BufferedReader(new FileReader(sourceFile));
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
            while ((line = reader.readLine()) != null) {
                String[] de = line.split("\\|");
                if (de[1].equals(id)) {
                    writer.write(line);
                    writer.newLine();
                }
            }
            reader.close();
            writer.close();
            sourceFile.delete();
            outputFile.renameTo(sourceFile);
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }


    @Override
    public User findById(UUID uuid) {
        String line = null;
        try {;
            File sourceFile = new File(fileName);
            BufferedReader reader = new BufferedReader(new FileReader(sourceFile));;
            while ((line = reader.readLine()) != null) {
                String[] de = line.split("\\|");
                if(UUID.fromString(de[0]).equals(uuid)) {
                    return new User(uuid, de[1], de[2], de[3], de[4]);
                }
            }
        }catch (IOException  e) {
            e.printStackTrace();
        }
        return null;
    }
}
