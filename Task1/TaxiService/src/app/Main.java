package app;


import dto.SignUpForm;
import mappers.Mappers;
import models.User;
import repositories.UsersRepository;
import repositories.UsersRepositoryFilesImpl;
import services.UsersService;
import services.UsersServiceImpl;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);

        usersService.signUp(new SignUpForm("Марсель", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty007"));
        usersService.signUp(new SignUpForm("Усов", "Егор",
                "egorusov@gmail.com", "qwerty123"));
        usersService.signUp(new SignUpForm("Усова", "Дарья",
                "dasha@gmail.com", "qwerty007123"));
        usersService.signUp(new SignUpForm("Иванова", "Олеся",
                "lesya@gmail.com", "qwerty007098"));
        usersService.signUp(new SignUpForm("Петя", "Куркин",
                "petya@gmail.com", "qwerty999"));


        System.out.println(usersRepository.findAll());
        User user1 = new User("Евгений",  "Арендт", "evgeniy@gmail.com", "qwerty777");
        usersRepository.save(user1);
        User user2 = new User("Петя", "Куркин",
                "petya@gmail.com", "qwerty999");
        usersRepository.delete1(user2);
        User user11 = new User("Евгений",  "Арендт", "evgeniy@gmail.com", "qwerty000");
        usersRepository.update(user11);
        usersRepository.deleteById(UUID.fromString("e21f95c8-02b1-4fab-a14a-2e08f83c9116"));
        usersRepository.findById(UUID.fromString("51fe7be3-b85c-4572-8ec6-8de0aac5f63c"));
    }
}